#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © Nekokatt 2019-2020
#
# This file is part of Hikari.
#
# Hikari is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hikari is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Hikari. If not, see <https://www.gnu.org/licenses/>.
"""Package metadata."""

from __future__ import annotations

__all__ = ["__author__", "__copyright__", "__docs__", "__email__", "__license__", "__url__", "__version__"]

__author__ = "Nekokatt"
__copyright__ = "© 2019-2020 Nekokatt"
__docs__ = "https://nekokatt.gitlab.io/hikari/staging/index.html"
__email__ = "3903853-nekokatt@users.noreply.gitlab.com"
__license__ = "LGPL-3.0-ONLY"
__url__ = "https://gitlab.com/nekokatt/hikari"
__version__ = "1.0.2"
